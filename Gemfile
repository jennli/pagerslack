# frozen_string_literal: true

source 'https://rubygems.org'
ruby '2.7.5'

Encoding.default_external = Encoding::UTF_8

gem 'activesupport', '~> 5.2.8'
gem 'async-websocket', '~>0.8.0'
gem 'concurrent-ruby', '~>1.1.10'
gem 'puma', '~> 4.3.12'
gem 'redis', '~> 4.1.4'
gem 'semantic_logger', '~> 4.7.0'
gem 'sinatra', '~> 2.2.3'
gem 'sinatra-contrib', '~> 2.2.3'
gem 'slack-ruby-client', '~>0.17.0'
gem 'yajl-ruby', '~>1.4.0', require: 'yajl'
gem 'gitlab-styles', '~> 6.6.0', require: false

group :development, :deployment do
  gem 'capistrano', '~> 3.10', require: false
  gem 'capistrano-bundler', '~> 2.0', require: false
  gem 'capistrano-rbenv', '~> 2.2', require: false
  gem 'capistrano3-puma'
end

group :test do
  gem 'rspec', '~> 3.5.0', require: false
  gem 'timecop', '~> 0.9.2'
  gem 'deprecation_toolkit', '~> 1.5.1'
end
