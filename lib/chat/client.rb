# frozen_string_literal: true

require 'concurrent-ruby'
require 'date'
require 'singleton'
require './lib/config'
require './lib/store'
require 'semantic_logger/loggable'

module Chat
  class Client
    include Singleton
    include SemanticLogger::Loggable
    EOD_PADDING = 30 # minutes; add extra time before the EOD for individuals
    RATE_LIMIT_DELAY = 1 # seconds; workaround to avoid rate limiting
    LAST_PRESENCE_CALL = Concurrent::AtomicReference.new(Time.now)
    FakeMessage = Struct.new(:ts)

    def message(message, ts: nil)
      if Config.slack_output
        client.chat_postMessage(channel: Config.channel, text: message, as_user: true, thread_ts: ts)
      else
        logger.debug(payload: { message: message, message_in_thread: ts })

        FakeMessage.new(Time.now.to_f)
      end
    end

    def pm_message(message, user:, force: false)
      if Config.slack_output || force
        client.chat_postMessage(channel: user, text: message, as_user: true)
      else
        logger.debug(payload: { message: message, message_in_thread: false, to_user: user })

        FakeMessage.new(Time.now.to_f)
      end
    end

    def update_message(message, ts:)
      if Config.slack_output
        client.chat_update(channel: channel_id, text: message, as_user: true, ts: ts)
      else
        logger.debug(payload: { message: message, updated_message: ts })

        FakeMessage.new(Time.now.to_f)
      end
    end

    def slack_api_secret=(secret)
      Store.set(:slack_api_secret, secret)
    end

    def members(members = [], cursor = nil)
      return members if cursor == ''

      data = client.conversations_members(channel: channel_id, cursor: cursor)
      members += data.members

      members(members, data.response_metadata.next_cursor)
    end

    def online?(member, retries: 10)
      wait_if_needed

      # Start with the less expensive:
      # Tier 4, Tier 3, Tier 3 API limits
      available_status?(member) && active?(member) && !dnd?(member)
    rescue Slack::Web::Api::Errors::TooManyRequestsError
      sleep RATE_LIMIT_DELAY

      retry unless (retries -= 1).zero?
    end

    def get_username(member)
      user_info(member).user.name
    end

    def delete_message(ts)
      return unless Config.slack_output

      client.chat_delete(channel: channel_id, ts: ts)
    end

    def dnd?(member, retries: 5)
      dnd = client.dnd_info(user: member)
      latest_available = Time.at(dnd.next_dnd_start_ts) - EOD_PADDING * 60
      dnd.dnd_enabled && Time.now.utc > latest_available.utc
    rescue Slack::Web::Api::Errors::TooManyRequestsError
      sleep RATE_LIMIT_DELAY

      retry unless (retries -= 1).zero?
    end

    def get_user_id(email:)
      client.users_lookupByEmail(email: email)&.user&.id
    end

    def invite(members)
      client.conversations_invite(channel: channel_id, users: members.join(','))
    rescue Slack::Web::Api::Errors::AlreadyInChannel
      # nothing to be done here
    end

    def channel_id
      # TODO: Use a more efficient way to get the channel ID
      @channel_id ||=
        begin
          channels = client.users_conversations(types: :public_channel).channels

          channels.find do |channel|
            channel.name == Config.channel[1..]
          end.id
        end
    end

    private

    def active?(member)
      client.users_getPresence(user: member)[:presence] == 'active'
    end

    def available_status?(member)
      !Config.unavailable_emojis.include?(user_info(member).profile&.status_emoji)
    end

    def user_info(member)
      client.users_info(user: member)
    end

    def title_for(member)
      user_info(member).profile&.title
    end

    def slack_api_secret
      Store.get(:slack_api_secret)
    end

    def client
      @client ||=
        Slack.configure do |config|
          config.token = slack_api_secret
          raise 'Missing API token' unless config.token
        end

      Slack::Web::Client.new
    end

    def wait_if_needed
      now = Time.now.utc
      last = LAST_PRESENCE_CALL.get_and_set(now)

      sleep RATE_LIMIT_DELAY if (now - last) < RATE_LIMIT_DELAY
    end
  end
end
