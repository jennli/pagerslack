# frozen_string_literal: true

namespace :stats do
  task :download do
    on roles(:all) do |_host|
      within current_path.to_s do
        execute :bundle, :exec, 'bin/parser -f log/pagerslack.json.log'
        download! 'pagerslack_parsed.csv', 'pagerslack_parsed.csv'
      end
    end
  end
end
