# frozen_string_literal: true

require 'spec_helper'
require 'semantic_logger'
require './lib/log_parser'

describe LogParser do
  subject(:parser) { described_class.new(file: './spec/parser.log.test') }

  it 'works' do
    expect(subject.parse).to eq([{ attempts: 6,
                                   weekend_pinged: false,
                                   unavailable: false,
                                   incident_url: 'https://gitlab.com/issue/123',
                                   reported_by: 'reporter',
                                   reported_at: '2020-11-05T19:05:00.204402Z',
                                   time_to_response: 724_746.6955929995,
                                   time_at_response: '2020-11-05T19:17:05.318236Z' }])
  end
end
