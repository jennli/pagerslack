# frozen_string_literal: true

require 'spec_helper'
require 'semantic_logger'
require './lib/config'
require './lib/holiday_checker'
require './lib/chat/ping_forecaster'
require './lib/chat/message_parser'
require 'slack'

describe Chat::MessageParser do
  describe '#parse' do
    let(:event) { double('event').as_null_object }
    let(:client) { double('client').as_null_object }

    before do
      allow_any_instance_of(Chat::PingForecaster).to receive(:oncall_list).with(any_args).and_return({ 'member' => [{ 'name': 'member', 'slack': 'member', weekend: 'No' }] })
      allow_any_instance_of(HolidayChecker).to receive(:today?).and_return(false)
      allow_any_instance_of(described_class).to receive(:client).and_return(client)
      allow_any_instance_of(Chat::Members).to receive(:client).and_return(client)

      stub_const('HolidayChecker::HOLIDAYS_FILENAME', './holidays.yml.example')
    end

    it 'works' do
      message_parser = described_class.new(event, { 'text' => 'position', 'user' => 'user' })

      result = message_parser.parse

      expect(result).not_to be_nil
    end

    context 'with an incident' do
      it 'works with a URL' do
        allow_any_instance_of(Chat::Members).to receive(:oncall_list).and_return({ 'member' => [{ 'name': 'member' }] })

        message_parser = described_class.new(event, { 'text' => 'https://url', 'user' => 'member', 'command' => Config.command })

        expect(client).to receive(:message).with('<@member> thanks for reporting a <https://url|new incident>. Bear with me while I find someone to help.')

        message_parser.parse
      end

      it 'without a URL does not work' do
        message_parser = described_class.new(event, { 'user' => 'member', 'command' => Config.command })

        expect(client).to receive(:pm_message).with('<@member> please pass an incident URL: `/devoncall https://incident.url`', user: 'member')

        message_parser.parse
      end
    end
  end
end
