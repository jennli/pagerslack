# frozen_string_literal: true

require 'spec_helper'
require 'semantic_logger'
require './lib/chat/follow_up_poster'
require 'slack'

describe Chat::FollowUpPoster do
  let(:members) { %w[one two] }
  let(:one_message) { 'Hey <@one>, I noticed you did not respond to the incident in <#channel>. Please note responding to an incident is of <https://about.gitlab.com/handbook/on-call/#expectations-for-on-call|higher priority> than meetings or other work unless you are working on a S1. Please remember to update your <https://tinyurl.com/yxjvstzr|Slack working hours and notification settings> and know that you can use `top` and `position` here to check your position in the queue.' }
  let(:two_message) { 'Hey <@two>, I noticed you did not respond to the incident in <#channel>. Please note responding to an incident is of <https://about.gitlab.com/handbook/on-call/#expectations-for-on-call|higher priority> than meetings or other work unless you are working on a S1. Please remember to update your <https://tinyurl.com/yxjvstzr|Slack working hours and notification settings> and know that you can use `top` and `position` here to check your position in the queue.' }

  subject(:poster) { described_class.new(members) }

  describe '#post' do
    it 'sends the right message' do
      allow(Chat::Client.instance).to receive(:channel_id).and_return('channel')

      expect(Chat::Client.instance).to receive(:pm_message).with(one_message, user: members.first).once
      expect(Chat::Client.instance).to receive(:pm_message).with(two_message, user: members.last).once

      poster.post
    end
  end
end
