# frozen_string_literal: true

# config valid for current version and patch releases of Capistrano
lock '~> 3.14.1'

set :application, 'pagerslack'
set :repo_url, 'git@gitlab.com:jameslopez/pagerslack.git'
set :rbenv_type, :user # or :system, or :fullstaq (for Fullstaq Ruby), depends on your rbenv setup
set :rbenv_ruby, File.read('.ruby-version').strip

set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w[rake gem bundle ruby rails puma pumactl]
set :rbenv_roles, :all # default value

set :linked_dirs, %w[log tmp/backup tmp/pids tmp/cache tmp/sockets vendor/bundle .bundle]

set :puma_bind, %w[tcp://127.0.0.1:9292 unix:///tmp/puma.sock]

# Default branch is :master
# set :branch, 'new-deploy'

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, "/var/www/my_app_name"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, "config/database.yml"

append :linked_files, 'oncall.csv'
append :linked_files, 'new_oncall.csv'
append :linked_files, 'holidays.yml'
append :linked_files, 'exclusions.yml'

# Default value for linked_dirs is []
# append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

set :default_env, {
  SLACK_CLIENT_ID: ENV['SLACK_CLIENT_ID'],
  SLACK_API_SECRET: ENV['SLACK_API_SECRET'],
  SLACK_REDIRECT_URI: ENV['SLACK_REDIRECT_URI'],
  SLACK_VERIFICATION_TOKEN: ENV['SLACK_VERIFICATION_TOKEN']
}

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
# set :keep_releases, 5

# Uncomment the following to require manually verifying the host key before first deploy.
# set :ssh_options, verify_host_key: :secure
#
# after :log_revision,:restart
#
# namespace :deploy do
#   task :restart do
#     on roles(:app), in: :groups, limit: 3, wait: 10 do
#       wiin release_path do
#         invoke 'bundle exec rackup -p 9292'
#       end
#     end
#   end
# end
